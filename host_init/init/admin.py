from django.contrib import admin

# Register your models here.
from .models import host

class hostadmin(admin.ModelAdmin):
    list_display = ('ip', 'user','port', 'function', 'date', 'desc',)
    search_fields = ('ip',)
    list_filter = ('ip',)

admin.site.register(host, hostadmin)
admin.AdminSite.site_header = '服务器初始化系统'
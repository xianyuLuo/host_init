from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver
import os

# Create your models here.
INIT_FUNCTIONS = {
    ('add_disk', '挂载磁盘'),
    ('add_ly_ops', '添加ly_ops_key'),
    ('add_zabbix_monitor', 'zabbix_monitor'),
}

class host(models.Model):
    ip = models.CharField('IP地址', max_length = 200, blank = False, help_text = '服务器IP地址，如有多个，请用";"分号分隔！')
    user = models.CharField('用户', max_length = 100, blank = False, default = 'root', help_text = '连接用户，如多个IP用户不同，请多次操作')
    port = models.IntegerField('端口', blank = False, default = 22, help_text = '服务器端口')
    password = models.CharField('密码', max_length = 50, blank = False, help_text = '服务器密码')
    function = models.CharField('初始化功能', max_length = 200, blank = False, choices = INIT_FUNCTIONS, help_text = '请选择功能')
    date = models.DateTimeField('更新时间', auto_now_add = True)
    desc = models.TextField('备注', max_length = 100, blank = True)

    def __str__(self):
        return self.ip

    class Meta:
        verbose_name = '服务器初始化'
        verbose_name_plural = verbose_name
        db_table = 'i_host'

@receiver(post_save, sender = host)
def host_init(sender, **kwargs):
    test = list(host.objects.all().order_by('-date').values('ip', 'user', 'port', 'password', 'function'))[0]
    ip = test['ip'].split(";")
    user = test['user']
    port = test['port']
    password = test['password']
    function = test['function']

    for i in range(len(ip)):
        print (ip[i])
        os.system('echo %s')
    print (user, ip, port, password)